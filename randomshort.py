#!/usr/bin/python3


import webapp
import random
import string
import urllib.parse

class randomshortApp (webapp.webApp):

    # Declare and initialize content
    urls = {
    }

    def parse(self, request):
        """Return the resource name (including /)"""
        metodo = request.split(' ', 2)[0]
        recurso = request.split(' ', 2)[1]
        cuerpo = request.split('\n')[-1]
        return (metodo,recurso,cuerpo)

    def process(self, petition):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """
        metodo,recurso,cuerpo = petition

        form = """
            <br/><br/>
            <form action="" method = "POST"> 
            <input type="text" name="url" value="">
            <input type="submit" value="Send">
             </form>
        """
        if metodo == "GET":
            if recurso == "/":
                httpCode = "200 OK"
                htmlBody = "<html><body> <h1> Shortener</h1> Please write url to short: <br> " + form + "<br>" \
                           + "<p> shortened urls:</p>" + str(self.urls) + "</body></html>"
            elif recurso in self.urls.keys():
                httpCode = "301 Permanent Redirect"
                httpBody = "<html><body> Url was found! Redirecting to the website" \
                           + self.urls[recurso] + "<meta http-equiv='refresh' content='3;URL=" + self.urls[
                               recurso] + "'></body></html>"
            else:
                httpCode = "400 Bad Request"
                htmlBody = "<html><body>Url doesnt exist on this website. Do you want to go for the main page?: <br>" \
                            + "<a href='/'> Main Page</a> </body></html>"
        elif metodo == "POST":
            url = urllib.parse.unquote((cuerpo.split('=')[1]))
            if 'http' in url:
                url = url.split('//')[1]
            if "/" in url:
                recursoForm = url.split('/')[-1]
                recursoForm = '/' + recursoForm
                if  recursoForm in self.urls.keys():
                    httpCode = "301 Permanent Redirect"
                    htmlBody = "<html><body> Url was shorted before, redirecting to the website: " \
                    + "<br> <meta http-equiv='refresh' content='3;URL=" + self.urls[recursoForm] + "'></body></html>"

                else:
                    httpCode = "404 Not Found"
                    htmlBody = "<html><body>Url shorted its not from here. Click on return button to return to the main page: " \
                               '<a href="/">Return</a>' + "</body></html>"
            else:
                if url != "": # add urls to dict
                    if not url.startswith('http://') and not url.startswith('https://'):
                        url = "https://" + url
                    urlrand = ''.join(random.choice(string.ascii_letters + string.digits) for i in range(10))
                    urlrand = '/' + urlrand
                    if url not in self.urls.values():
                        self.urls[urlrand] = url
                        httpCode = "200 OK"
                        htmlBody = "<html><body> <h1> Url was added before</h1> Do you want to add another urls to short: <br> " \
                                   + form + "<br> <p> shortened urls:</p>" + str(self.urls) + "</body></html>"
                    else:
                        httpCode = "200 OK"
                        htmlBody = "<html><body><h1> Url was added previously. </h1> Do you want to add other url to short?: <br> "\
                               + form + "<br> <p> shortened urls:</p>" + str(self.urls) + "</body></html>"

                else:
                    httpCode = "400 Bad Request"
                    htmlBody = "<html><body>ERROR: Form is empty. Do you want to go for the main page?:" \
                       " <br>" + "<a href='/'> Main Page</a> </body></html>"
        return (httpCode, htmlBody)

if __name__ == "__main__":
    testWebApp = randomshortApp("127.0.0.1",1234)
